const express = require('express');
var router = express.Router();
var ObjectId  = require('mongoose').Types.ObjectId;
 

var {Patient} = require('../models/patient.js');


//  get all patient details
router.get('/',(req,res) =>{
    Patient.find((err, docs) => {
        if(!err) {res.send(docs);}
        else{console.log ('error in retriving  Patient:' + JSON.stringify(err, undefined, 2));}
    });
});

// get single patient details
router.get('/:id', (req, res) =>{
    if(!ObjectId.isValid(req.params.id))
    return res.status(400).send(`no record found with given id :${req.params.id}`);

    Patient.findById(req.params.id, (err, doc)=>{
        if(!err) {res.send(doc);}
        else {console.log("error in retriving data of Patient :" +JSON.stringify(err, undefined, 2));}
    });

});

//  save patient data
router.post('/',(req,res) =>{
    var pat = new Patient({
        name: req.body.name,
        address: req.body.address,
        phonenumber: req.body.phonenumber
    });
    pat.save((err,doc)=> {
        if(!err){res.send(doc);}
        else{
            console.log('error in patient saving :' +JSON.stringify(err,undefined, 2));
        }
    })
});

// update patient data
router.put('/:id', (req, res)=>{
    if(!ObjectId.isValid(req.params.id))
    return res.status(400).send(`there is no record with this id :${req.params.id}`);

    var pats = {
        name: req.body.name,
        address: req.body.address,
        phonenumber: req.body.phonenumber
    };

    Patient.findByIdAndUpdate(req.params.id,{ $set: pats}, {new: true }, (err, doc) =>{
        if(!err) {res.send(doc);}
        else{ console.log("error in Patient update :" +JSON.stringify(err, undefined, 2));}

    });
});

// delete patient data
router.delete('/:id', (req,res) => {
    if(!ObjectId.isValid(req.params.id))
    return res.status(400).send(`there is no record with this id :${req.params.id}`);

    Patient.findByIdAndRemove(req.params.id, (err, doc)=>{
        if(!err){res.send(doc);}
        else{console.log('error in  deleting  patient :' +JSON.stringify(err,undefined,2));}
    });
});

module.exports = router;