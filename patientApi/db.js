let mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/crud', (err)=> {
    if(!err)
    console.log('mongoDB Connection Successful...');
    else
    console.log('error in DB Connection:' +JSON.stringify(err,undefined,2));
});

module.exports = mongoose;  