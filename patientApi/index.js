const express = require('express');
 const bodyParser = require('body-parser');
 let cors = require('cors');

 let { mongoose } =  require('./db.js');
 var patientController = require('./controllers/patientController.js');

 var app = express();
 app.use(bodyParser.json());
 app.use(cors({'Access-Control-Allow-Origin':'*'}));
 app.listen(3000,()=> console.log('server started at port : 3000'));

 app.use('/patient', patientController )