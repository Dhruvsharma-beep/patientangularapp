const mongoose = require('mongoose');
var Patient = mongoose.model('Patient',{
    name: {type: String},
    address: {type: String},
    phonenumber: {type:Number},
});

module.exports = {Patient};