import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import { materialize } from 'rxjs/operators';

import { PatientService} from '../modelservice/patient.service';
import { Patient } from '../modelservice/patient.model';

declare var M:any;
@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css'],
  providers:[PatientService]
})
export class PatientComponent implements OnInit {
  constructor(public patientservice: PatientService) { }

  ngOnInit(): void {
    this.resetForm();
    this.getAllPatientList();
  }

    resetForm(form?: NgForm){
      if(form)
      this.resetForm();
      this.patientservice.selectedPatient = {
        _id: "",
        name: "",
        address: "",
        phonenumber:"", 
      }
    }

   
    onSubmit(form: NgForm){

    if(form.value._id == ""){
        this.patientservice.postPatient(form.value).subscribe((res)=>{
          this.getAllPatientList();
          M.toast({html: 'Inserted Successfully!', classes: 'btn btn-success'});
          this.resetForm(form);
        });
    }
   else{
      this.patientservice.putPatient(form.value).subscribe((res)=>{ 
        this.getAllPatientList();
        M.toast({html: 'updated Successfully!', classes: 'btn btn-primary'}); 
        this.resetForm(form);
    });
  }
}

    getAllPatientList(){
      this.patientservice.getAllPatient().subscribe((res)=>{
        this.patientservice.patients = res as Patient[];
        
      });
    }
    onEdit(pat: Patient){
      this.patientservice.selectedPatient = pat;
    }
    onDelete(_id: String, form: NgForm){
      if(confirm('Are you sure to delete this record ?') == true){
        this.patientservice.deletePatient(_id).subscribe((res)=>{
          this.getAllPatientList();
          this.resetForm(form);
          M.toast({html: 'deleted Successfully!', classes: 'btn btn-danger'}); 
        });         
      }
    }
}
