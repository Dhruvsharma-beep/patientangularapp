import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs'; 
import { map } from 'rxjs/operators';
import {Patient} from './patient.model';

@Injectable({
  providedIn: 'root'
})
export class PatientService {

  // to avoid strict mode i have added ! symbol
  selectedPatient!:Patient; 
  patients!:Patient[];
   baseUrl = "http://localhost:3000/patient";


  constructor(private http:HttpClient) { }

  postPatient(pat: Patient){
    return this.http.post(this.baseUrl, pat);
  }
  getAllPatient(){
    return this.http.get(this.baseUrl);
  }
  putPatient(pat: Patient){
    return this.http.put(this.baseUrl + `/${pat._id}`, pat);
  }
  deletePatient(_id: String){
    return this.http.delete(this.baseUrl + `/${_id}`);
  }
}
